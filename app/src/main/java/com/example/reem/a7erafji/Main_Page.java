package com.example.reem.a7erafji;

import android.app.job.JobInfo;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main_Page extends AppCompatActivity {

    Button post_job, unrated_jobs_btn;
    private static String TAG = Main_Page.class.getSimpleName();
    private static final int REQUEST_JOBPOST = 1;
    private int current_user_id;
    CustomListAdapterJob clad;
    private SharedPreferences sharedPref;
    private static String url_get_job = "http://192.168.91.1/android_connect/get_jobs_user.php";
    SharedPreferences.Editor editor;
    TextView my_profile;
    ImageView comC,paintC, electC, waterC, happC, cleanC;
    private ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(Main_Page.this);
        editor = sharedPref.edit();
        final String token = FirebaseInstanceId.getInstance().getToken();
        editor.putString("my_token", token);
        editor.apply();
        Log.d ("my token: ", token);
        setContentView(R.layout.activity_main__page2);

        lv = (ListView) findViewById(R.id.custom_list);
        getListData();
        current_user_id = sharedPref.getInt("current_user_id", 1);
        post_job = (Button) findViewById(R.id.button2);
        unrated_jobs_btn = (Button) findViewById(R.id.unrated_jobs_btn);
        my_profile = (TextView) findViewById(R.id.my_profilee);
        my_profile.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                editor.putInt("profile_id", sharedPref.getInt("current_user_id", 1));
                editor.apply();
                Log.d ("response: ", "here");
                Intent intent = new Intent(getApplicationContext(), Profile.class);
                startActivityForResult(intent, REQUEST_JOBPOST);
            }
        });


        final String[] listItems = getResources().getStringArray(R.array.categories);
        post_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(Main_Page.this)
                        .setTitle("Choose a Job Category")
                        .setSingleChoiceItems(listItems, 0, null)
                        .setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                int i = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                                editor.putString("category",listItems[i]);
                                editor.putInt("categoryID",i +1);
                                editor.apply();
                                Toast.makeText(Main_Page.this, "Category Selected: " + listItems[i],
                                        Toast.LENGTH_SHORT).show();
                                Log.d ("response: category id", Integer.toString(i +1));
                                dialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(), PostJob.class);
                                startActivityForResult(intent, REQUEST_JOBPOST);                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        unrated_jobs_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), UnRatedJobs.class);
                startActivityForResult(intent, REQUEST_JOBPOST);
            }
        });


        comC =  (ImageView)findViewById(R.id.Computer_Technician);
        paintC =  (ImageView)findViewById(R.id.House_Painting_or_Decorating);
        electC =  (ImageView)findViewById(R.id.Electrician);
        waterC =  (ImageView)findViewById(R.id.Plumber_or_Water_Technician);
        happC =  (ImageView)findViewById(R.id.Home_Appliances_Technician);
        cleanC =  (ImageView)findViewById(R.id.House_Cleaning);

        comC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("categoryID",1);
                editor.putString("category", "Computer Technician");
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), view_suppliers.class);
                startActivityForResult(intent, 1);
            }
        });
        paintC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("categoryID",2);
                editor.putString("category", "House Painting or Decorating");
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), view_suppliers.class);
                startActivityForResult(intent, 1);
            }
        });
        electC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("categoryID",3);
                editor.putString("category", "Electrician");
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), view_suppliers.class);
                startActivityForResult(intent, 1);
            }
        });
        waterC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("categoryID",4);
                editor.putString("category", "Plumber or Water Technician");
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), view_suppliers.class);
                startActivityForResult(intent, 1);
            }
        });
        happC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("categoryID",5);
                editor.putString("category", "Home Appliances Technician");
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), view_suppliers.class);
                startActivityForResult(intent, 1);
            }
        });
        cleanC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editor.putInt("categoryID",6);
                editor.putString("category", "House Cleaning");
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), view_suppliers.class);
                startActivityForResult(intent, 1);
            }
        });


    }
    @Override
    public void onResume(){
        super.onResume();
        Log.d ("deleted:", "returned?");
        getListData();
    }

    private void update (ArrayList<jobInfo> a){
        final ArrayList image_details = a;
        clad = new CustomListAdapterJob(Main_Page.this, image_details);
        lv.setAdapter(clad);
    }


    public void getListData() {
        final ArrayList<jobInfo>  results = new ArrayList<>();
        String URL = url_get_job;
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        try {
                            Log.d ("response: ", "succcess");
                            Log.d ("response", Response);
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response ", Integer.toString(response.length()));
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                jobInfo temp = new jobInfo();
                                temp.setTitle( person.getString("title"));
                                temp.setDescription( person.getString("description"));
                                temp.setDeadline( person.getString("date"));
                                temp.setCategory( person.getString("category"));
                                temp.setJobID(Integer.parseInt(person.getString("id")));
                                Log.d ("deleted:", "here?");
                                results.add (temp);
                            }
                            update (results);
                        } catch (JSONException e) {
                            Log.d ("response: ", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userid", Integer.toString(sharedPref.getInt("current_user_id", 1)));
                return params;
            }
        };
        // Adding request to request queue
        Volley.newRequestQueue(Main_Page.this).add(req);
    }
}
