
package com.example.reem.a7erafji;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.reem.a7erafji.R.drawable.man;

public class Profile extends AppCompatActivity {
    TextView nameP, location;
    RatingBar EttR, DescR, PayR, OverR;
    String CurUser, Profile_id;
    private final int IMG_REQUEST = 1;
    ListView LV;
    ImageView pp, edit;
    Bitmap bitmap;
    CustomAdapterProfile customAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        nameP = (TextView)findViewById(R.id.name);
        location = (TextView)findViewById(R.id.location);
        EttR = (RatingBar) findViewById(R.id.etiquetteBar);
        PayR = (RatingBar) findViewById(R.id.paymentBar);
        DescR = (RatingBar) findViewById(R.id.descriptionBar);
        OverR = (RatingBar) findViewById(R.id.overallBar);
        pp = (ImageView) findViewById(R.id.profile_image);
        edit = (ImageView) findViewById(R.id.editt);
        LV= (ListView) findViewById(R.id.custom_list);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(Profile.this);
        SharedPreferences.Editor editor = sharedPref.edit();
        CurUser =  Integer.toString(sharedPref.getInt("current_user_id", 1));
        Profile_id =  Integer.toString(sharedPref.getInt("profile_id", 1));
        editor.apply();

        if (!CurUser.equals(Profile_id))
            edit.setVisibility(View.GONE);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),EditProfile.class);
                startActivity(i);
            }
        });

        Data();
        getData();
    }
    String URL = "http://192.168.91.1/android_connect/user_data.php";



    public void Data () {



        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", Profile_id);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URL, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response: ", response.toString());
                try {
                    String temp = response.getString("success");
                    if (temp.equals("0")) {
                        Log.d("Response: ", "failed");
                    } else {
                        Log.d("Response: ", "success");
                        nameP.setText(response.getString("name"));
                        location.setText(response.getString("Location"));
                        EttR.setRating(Float.parseFloat(response.getString("Rating1")));
                        PayR.setRating(Float.parseFloat(response.getString("Rating2")));
                        DescR.setRating(Float.parseFloat(response.getString("Rating3")));
                        OverR.setRating(Float.parseFloat(response.getString("overallRating")));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError response) {
                Log.d("Response: ", response.toString());
            }
        });
        Volley.newRequestQueue(this).add(jsObjRequest);

        String url = "http://192.168.91.1/android_connect/upload/";
        url = url + Profile_id +".jpg";
        ImageRequest imgReq = new ImageRequest(url, new Response.Listener<Bitmap>(){

            @Override
            public void onResponse(Bitmap response){
                pp.setImageBitmap(response);
            }

        }, 0,0,ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pp.setImageResource(R.drawable.man);
            }
        });
        MySingleton.getInstance(Profile.this).addToRequestQueue(imgReq);
    }

    public void getData () {


        final ArrayList<jobInfo>  results = new ArrayList<>();
        String URL = "http://192.168.91.1/android_connect/get_rated_jobs.php";
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        try {
                            Log.d ("response rated: ", "succcess");
                            Log.d ("response", Response);
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response ", Integer.toString(response.length()));
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                jobInfo tmp = new jobInfo();
                                tmp.setTitle( person.getString("title"));
                                tmp.setSupName( person.getString("name"));
                                tmp.setSupID(person.getInt("rating_user"));
                                tmp.setR1( Float.parseFloat(person.getString("rating1")));
                                tmp.setR2(Float.parseFloat(person.getString("rating2")));
                                tmp.setR3(Float.parseFloat(person.getString("rating3")));
                                tmp.setDescription(person.getString("comment"));
                                results.add (tmp);
                            }
                            Log.d ("res", Integer.toString(results.size()));
                            update (results);
                        } catch (JSONException e) {
                            Log.d ("response: ", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", Profile_id);
                return params;
            }
        };
        // Adding request to request queue
        Volley.newRequestQueue(Profile.this).add(req);

    }

    private void update (ArrayList<jobInfo> a){
        final ArrayList image_details = a;
        customAdapter = new Profile.CustomAdapterProfile(Profile.this, image_details);
        LV.setAdapter(customAdapter);
    }

    class CustomAdapterProfile extends BaseAdapter {

        private ArrayList<jobInfo> listData;
        private LayoutInflater layoutInflater;
        private static final int VIEW_JOBPOST = 3;
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        Context temp;

        public CustomAdapterProfile(Context aContext, ArrayList<jobInfo> listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(aContext);
            sharedPref = PreferenceManager.getDefaultSharedPreferences(aContext);
            editor = sharedPref.edit();
            temp = aContext;
        }
        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public final Object getItem(int position) {
            return listData.get(position);
        }

        @Override
        public final long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final CustomAdapterProfile.ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.profile_ratings_custom, null);
                holder = new CustomAdapterProfile.ViewHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.name= (TextView) convertView.findViewById(R.id.name);
                holder.description= (TextView) convertView.findViewById(R.id.Desc);
                holder.er = (RatingBar) convertView.findViewById(R.id.etBar);
                holder.dr= (RatingBar) convertView.findViewById(R.id.descBar);
                holder.pr= (RatingBar) convertView.findViewById(R.id.payBar);
                holder.pp = (ImageView) convertView.findViewById(R.id.profile_image);
                convertView.setTag(holder);
            } else {
                holder = (CustomAdapterProfile.ViewHolder) convertView.getTag();
            }

            holder.title.setText(listData.get(position).getTitle());
            holder.name.setText(listData.get(position).getSupName());
            holder.description.setText("How was your experience with " + nameP.getText() + "?\n" +listData.get(position).getDescription());
            holder.er.setRating(listData.get(position).getR1());
            holder.pr.setRating(listData.get(position).getR2());
            holder.dr.setRating(listData.get(position).getR3());
            String url = "http://192.168.91.1/android_connect/upload/";
            url = url + listData.get(position).getSupID() +".jpg";

            ImageRequest imgReq = new ImageRequest(url, new Response.Listener<Bitmap>(){

                @Override
                public void onResponse(Bitmap response){
                    holder.pp.setImageBitmap(response);
                }

            }, 0,0,ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pp.setImageResource(R.drawable.man);
                }
            });
            MySingleton.getInstance(Profile.this).addToRequestQueue(imgReq);
            return convertView;
        }

        class ViewHolder {
            TextView title;
            TextView name;
            TextView description;
            RatingBar er;
            RatingBar dr;
            RatingBar pr;
            ImageView pp;
        }
    }

}



