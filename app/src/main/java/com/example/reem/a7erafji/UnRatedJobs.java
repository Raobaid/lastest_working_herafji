package com.example.reem.a7erafji;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.R.id.list;

public class UnRatedJobs extends AppCompatActivity {

    ListView LV;
    String CurUser;
    final String IP = "192.168.91.1";
    String url = "http://192.168.91.1/android_connect/get_unrated_jobs.php";
    CustomAdapter customAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_un_rated_jobs);
        LV= (ListView) findViewById(R.id.custom_list);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(UnRatedJobs.this);
        SharedPreferences.Editor editor = sharedPref.edit();
        CurUser =  sharedPref.getString("current_user_id", "1");
        editor.apply();
        getData();
    }

    public void getData () {


        final ArrayList<jobInfo>  results = new ArrayList<>();
        String URL = url;
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        try {
                            Log.d ("response: ", "succcess");
                            Log.d ("response", Response);
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response ", Integer.toString(response.length()));
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                jobInfo tmp = new jobInfo();
                                tmp.setTitle( person.getString("title"));
                                tmp.setSupName( person.getString("name"));
                                tmp.setSuprate( Float.parseFloat(person.getString("overallRating")));
                                tmp.setVisitID(Integer.parseInt(person.getString("id")));
                                tmp.setJobID(Integer.parseInt(person.getString("Jobposts_id")));
                                tmp.setSupID(Integer.parseInt(person.getString("supplier_id")));
                                results.add (tmp);
                            }
                            Log.d ("res", Integer.toString(results.size()));
                            update (results);
                        } catch (JSONException e) {
                            Log.d ("response: ", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", CurUser);
                return params;
            }
        };
        // Adding request to request queue
        Volley.newRequestQueue(UnRatedJobs.this).add(req);


    }

    private void update (ArrayList<jobInfo> a){
        final ArrayList image_details = a;
        customAdapter = new CustomAdapter(UnRatedJobs.this, image_details);
        LV.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter {

        private ArrayList<jobInfo> listData;
        private LayoutInflater layoutInflater;
        private static final int VIEW_JOBPOST = 3;
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        Context temp;

        public CustomAdapter(Context aContext, ArrayList<jobInfo> listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(aContext);
            sharedPref = PreferenceManager.getDefaultSharedPreferences(aContext);
            editor = sharedPref.edit();
            temp = aContext;
        }
        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public final Object getItem(int position) {
            return listData.get(position);
        }

        @Override
        public final long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final CustomAdapter.ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.unrated_jobs_custom_layout, null);
                holder = new CustomAdapter.ViewHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.name= (TextView) convertView.findViewById(R.id.name);
                holder.rate = (RatingBar) convertView.findViewById(R.id.rate);
                holder.cancel=(Button)convertView.findViewById(R.id.cancle);
                holder.delay=(Button)convertView.findViewById(R.id.delay);
                holder.ratebtn=(Button)convertView.findViewById(R.id.Rate);
                holder.image  = (ImageView) convertView.findViewById(R.id.profile_image);
                convertView.setTag(holder);
            } else {
                holder = (CustomAdapter.ViewHolder) convertView.getTag();
            }

            holder.title.setText(listData.get(position).getTitle());
            holder.name.setText(listData.get(position).getSupName());
            holder.rate.setRating(listData.get(position).getSuprate());

            String url = "http://192.168.91.1/android_connect/upload/";
            url = url + listData.get(position).getSupID() +".jpg";
            ImageRequest imgReq = new ImageRequest(url, new Response.Listener<Bitmap>(){

                @Override
                public void onResponse(Bitmap response){
                    holder.image.setImageBitmap(response);
                }

            }, 0,0,ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.image.setImageResource(R.drawable.man);
                }
            });
            MySingleton.getInstance(temp).addToRequestQueue(imgReq);


            holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder b = new AlertDialog.Builder(UnRatedJobs.this);

                    b.setMessage("Are you sure you don't want to rate this supplier?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Log.d ("response cancle title: ", listData.get(position).getTitle());
                                    String URL = "http://192.168.137.233/android_connect/cancle_visit_rate.php";
                                    StringRequest req = new StringRequest(Request.Method.POST, URL,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String Response) {
                                                    Log.d ("response: ", "succcess");
                                                    getData();
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    Log.d ("response: ", "error");
                                                    error.printStackTrace();
                                                }
                                            }
                                    ) {
                                        // here is params will add to your url using post method
                                        @Override
                                        protected Map<String, String> getParams() {
                                            Map<String, String> params = new HashMap<>();
                                            params.put("id", Integer.toString(listData.get(position).getVisitID()));
                                            return params;
                                        }
                                    };
                                    // Adding request to request queue
                                    Volley.newRequestQueue(UnRatedJobs.this).add(req);
                                }
                            }).setNegativeButton("No" , null);

                    AlertDialog alert = b.create();
                    alert.show();


                }
            });

            holder.delay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // go to specific page of job post
                    //  editor.putString ("title", listData.get(position).getTitle());
                    AlertDialog.Builder b = new AlertDialog.Builder(UnRatedJobs.this);
                    View dview = getLayoutInflater().inflate(R.layout.dialog_delay_rating,null);
                    final EditText days = (EditText)dview.findViewById(R.id.editText);
                    TextView t = (TextView) dview.findViewById(R.id.textView);
                    Button btn = (Button)dview.findViewById(R.id.btn);
                    Button btn2 = (Button)dview.findViewById(R.id.btn2);
                    b.setView(dview);
                    final AlertDialog a = b.create();

                    btn.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick (View view){
                            String URL = "http://" +IP + "/android_connect/delay_visit_rate.php";
                            StringRequest req = new StringRequest(Request.Method.POST, URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String Response) {
                                            Log.d ("response: ", "succcess");
                                            Log.d ("response", Response);
                                            getData();
                                            a.dismiss();
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.d ("response: ", "error");
                                            error.printStackTrace();
                                        }
                                    }
                            ) {
                                // here is params will add to your url using post method
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();
                                    params.put("id", Integer.toString(listData.get(position).getVisitID()));
                                    params.put("delay" , days.getText().toString());
                                    return params;
                                }
                            };
                            // Adding request to request queue
                            Volley.newRequestQueue(UnRatedJobs.this).add(req);
                        }
                    });

                    btn2.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick (View view){
                            a.dismiss();
                        }
                    });
                    a.show();
                }
            });

            holder.ratebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder b = new AlertDialog.Builder(UnRatedJobs.this);
                    View dview = getLayoutInflater().inflate(R.layout.rate_dialog,null);
                    final RatingBar tr = (RatingBar) dview.findViewById(R.id.ratetrust);
                    final RatingBar mr = (RatingBar) dview.findViewById(R.id.ratemanners);
                    final RatingBar cr = (RatingBar) dview.findViewById(R.id.ratecoop);
                    final EditText com = (EditText) dview.findViewById(R.id.comment);
                    Button rate = (Button) dview.findViewById(R.id.Rate);
                    Button cancel = (Button) dview.findViewById(R.id.cancle);

                    b.setView(dview);
                    final AlertDialog a = b.create();

                    rate.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick (View view){
                            String URL = "http://" + IP + "/android_connect/rate.php";
                            StringRequest req = new StringRequest(Request.Method.POST, URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String Response) {
                                            Log.d ("response: ", "succcess");
                                            Log.d ("response", Response);
                                            getData();
                                            a.dismiss();
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.d ("response: ", "error");
                                            error.printStackTrace();
                                        }
                                    }
                            ) {
                                // here is params will add to your url using post method
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();
                                    params.put("visit_id", Integer.toString(listData.get(position).getVisitID()));
                                    params.put("jobpost_id", Integer.toString(listData.get(position).getJobID()));
                                    params.put("sup_id", Integer.toString(listData.get(position).getSupID()));
                                    params.put("rating1", Float.toString(tr.getRating()));
                                    params.put("rating2", Float.toString(mr.getRating()));
                                    params.put("rating3", Float.toString(cr.getRating()));
                                    params.put("comment", com.getText().toString());
                                    return params;
                                }
                            };
                            // Adding request to request queue
                            Volley.newRequestQueue(UnRatedJobs.this).add(req);
                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick (View view){
                            a.dismiss();
                        }
                    });
                    a.show();
                }
            });

            return convertView;
        }

        class ViewHolder {
            TextView title;
            TextView name;
            RatingBar rate;
            ImageView image;
            Button cancel;
            Button delay;
            Button ratebtn;
        }
    }
}
