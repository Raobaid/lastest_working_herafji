package com.example.reem.a7erafji;

/**
 * Created by Reem on 5/11/2018.
 */

public class User {
    String name;
    String profileUrl;
    int id;
    public void setName(String a){this.name = a;}
    public void setProfileUrl (String a){this.profileUrl = a;}
    public void setUserID (int a){this.id = a;}
    public int getUserID(){return this.id;}
    public String getName (){return this.name;}
    public String getProfileUrl(){return this.profileUrl;}
}
