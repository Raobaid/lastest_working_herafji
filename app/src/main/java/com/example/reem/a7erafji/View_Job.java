package com.example.reem.a7erafji;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class View_Job extends AppCompatActivity {

    TextView title, description, category, deadline;
    private SharedPreferences sharedPref;
    ListView lv;
    CustomListAdapterJobView clad;
    private int job_id;
    private static String url_get_suppliers = "http://192.168.91.1/android_connect/get_suppliers_job.php";
    private static String url_get_supplier_info = "http://192.168.91.1/android_connect/get_suppliers_info.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_job);
        title = (TextView) findViewById(R.id.title);
        description= (TextView) findViewById(R.id.description);
        category = (TextView) findViewById(R.id.category);
        deadline = (TextView) findViewById(R.id.deadline);
        lv = (ListView) findViewById(R.id.custom_list);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        title.setText(sharedPref.getString("title", "title"));
        description.setText(sharedPref.getString("description", "description"));
        deadline.setText(sharedPref.getString("deadline", "deadline"));
        category.setText(sharedPref.getString("category", "category"));
        getListData();
        job_id = sharedPref.getInt("job_id", 1);
    }

    void update2 (  ArrayList<SupplierInfo> a ){
        final ArrayList image_details = a;
        clad = new CustomListAdapterJobView(View_Job.this, image_details);
        lv.setAdapter(clad);
    }

    private void update (ArrayList<String> supIds){
        final ArrayList<SupplierInfo> a  = new ArrayList<>();

        String query = "(";
        for (String x: supIds){
            query += x;
            query += ",";
        }
        query= query.substring(0,query.length()-1);
        query += ')';
        final String param = query;
        Log.d("response: ", query);
        String URL = url_get_supplier_info;
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        Log.d ("here : ", Response);
                        try {
                            Log.d ("response: ", "succcess");
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response ", Integer.toString(response.length()));
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                SupplierInfo temp = new SupplierInfo();
                                temp.setName(person.getString("name"));
                                temp.setCity(person.getString ("Location"));
                                temp.setID(person.getInt ("id"));
                                temp.setRating(person.getDouble("overallRating"));
                                a.add ( temp);
                            }
                            update2 (a);
                        } catch (JSONException e) {
                            Log.d ("response: ", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        Log.d ("here : ", "didnt work die");
                        error.printStackTrace();
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("param", param);
                return params;
            }
        };
        // Adding request to request queue
        Volley.newRequestQueue(View_Job.this).add(req);


    }


    private void getListData() {
        final ArrayList<String>  results = new ArrayList<>();
        String URL = url_get_suppliers;
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        try {
                            Log.d ("response: ", "succcess");
                            Log.d ("response", Response);
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response ", Integer.toString(response.length()));
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                results.add ( person.getString("supID"));
                            }
                            update (results);
                        } catch (JSONException e) {
                            Log.d ("response: ", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("job_id", Integer.toString(job_id));
                return params;
            }
        };
        // Adding request to request queue
        Volley.newRequestQueue(View_Job.this).add(req);
    }
}
