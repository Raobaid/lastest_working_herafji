package com.example.reem.a7erafji;

import android.widget.ImageView;

public class search_supplier {

    String name, city;
    int id;
    float rating;
    ImageView image;

    public  void setName (String a){this.name = a;}
    public void setCity (String a){this.city = a;}
    public void setId (int a){this.id = a;}
    public void setRating (float a){this.rating = a;}
    public void setImage(ImageView a){this.image = a;}

    public String getName(){return this.name;}
    public String getCity(){return this.city;}
    public int getId (){return this.id;}
    public  float getRating(){return this.rating;}
    public ImageView getImage(){return this.image;}
}
