package com.example.reem.a7erafji;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import junit.framework.Test;

import java.util.ArrayList;

public class CustomListAdapterJobView extends BaseAdapter {

    private ArrayList<SupplierInfo> listData;
    private LayoutInflater layoutInflater;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    Context temp;

    public CustomListAdapterJobView(Context aContext, ArrayList<SupplierInfo> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(aContext);
        editor = sharedPref.edit();
        temp = aContext;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public final Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public final long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.dialog_supplier_message, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.nameSup);
            holder.city = (TextView) convertView.findViewById(R.id.citySup);
            holder.rating = (TextView) convertView.findViewById(R.id.ratingSup);
            holder.message = (Button) convertView.findViewById(R.id.button);
            holder.image = (ImageView)  convertView.findViewById(R.id.imageSup);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(listData.get(position).getName());
        holder.city.setText(listData.get(position).getCity());
        holder.rating.setText(listData.get(position).getRating().toString());

        String url = "http://192.168.91.1/android_connect/upload/";
        url = url + listData.get(position).getSupID() +".jpg";
        ImageRequest imgReq = new ImageRequest(url, new Response.Listener<Bitmap>(){

            @Override
            public void onResponse(Bitmap response){
                holder.image.setImageBitmap(response);
            }

        }, 0,0,ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.image.setImageResource(R.drawable.man);
            }
        });
        MySingleton.getInstance(temp).addToRequestQueue(imgReq);

        holder.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // go to specific page of job post
                editor.putString ("sup_name", listData.get(position).getName());
                Log.d ("response id: ", Integer.toString(listData.get(position).getSupID()));
                editor.putInt("sup_id", listData.get(position).getSupID());
                editor.apply();
                Intent intent = new Intent(temp, message_list.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                temp.startActivity(intent);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView name;
        TextView city;
        TextView rating;
        ImageView image;
        Button message;
    }
}
