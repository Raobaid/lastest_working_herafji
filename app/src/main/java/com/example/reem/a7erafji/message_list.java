package com.example.reem.a7erafji;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.reem.a7erafji.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

public class message_list extends AppCompatActivity {

    final String SERVER_ADDRESS = "http://192.168.91.1/android_connect/notification.php?id=";

    TextView placeNameText;
    Button getPlaceButton;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private final static int PLACE_PICKER_REQUEST = 1;


    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    ArrayList <Message> messageList =  new ArrayList<>();
    private TextView nameSup, time, date;
    private LinearLayoutManager mLayoutManager;
    private ImageView image;
    private EditText input_text;
    private Button send_text, makeRequestBt;
    private static String url_post_message = "http://192.168.91.1/android_connect/post_message.php";
    private static String url_get_messages = "http://192.168.91.1/android_connect/get_messages.php";
    ScheduledExecutorService executorService;
    String location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        editor = sharedPref.edit();
        requestPermission();
        makeRequestBt = (Button) findViewById(R.id.makeRequestBt);
        image = (ImageView) findViewById(R.id.imageSup);
        final int supid =  sharedPref.getInt("sup_id", 1);
        String url = "http://192.168.91.1/android_connect/upload/";

        url = url + supid +".jpg";
        ImageRequest imgReq = new ImageRequest(url, new Response.Listener<Bitmap>(){

            @Override
            public void onResponse(Bitmap response){
                image.setImageBitmap(response);
            }

        }, 0,0,ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                image.setImageResource(R.drawable.man);
            }
        });
        MySingleton.getInstance(message_list.this).addToRequestQueue(imgReq);

        makeRequestBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                location = "no";
                final Dialog dialog = new Dialog(message_list.this);
                dialog.setContentView(R.layout.request);
                dialog.setTitle("Make a Job Request");
                dialog.getWindow().setLayout(1100, 700);

                placeNameText = (TextView) dialog.findViewById(R.id.placeName);
                getPlaceButton = (Button) dialog.findViewById(R.id.btGetPlace);
                time = (TextView) dialog.findViewById(R.id.time);
                date = (TextView) dialog.findViewById(R.id.date);
                placeNameText.setPaintFlags(placeNameText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


                time.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(message_list.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                time.setText( selectedHour + ":" + selectedMinute);
                            }
                        }, hour, minute, true);
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();

                    }
                });
                date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        //To show current date in the datepicker
                        Calendar mcurrentDate = Calendar.getInstance();
                        int mYear = mcurrentDate.get(Calendar.YEAR);
                        int mMonth = mcurrentDate.get(Calendar.MONTH);
                        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog mDatePicker;
                        mDatePicker = new DatePickerDialog(message_list.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                selectedmonth = selectedmonth + 1;
                                date.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
                            }
                        }, mYear, mMonth, mDay);
                        mDatePicker.setTitle("Select Date");
                        mDatePicker.show();
                    }
                });

                placeNameText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (location.charAt(0) == 'h') {
                            Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse((location).toString()));
                            startActivity(browser);
                        }
                    }
                });

                getPlaceButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                        try {
                            Intent intent = builder.build(message_list.this);
                            startActivityForResult(intent, PLACE_PICKER_REQUEST);
                        } catch (GooglePlayServicesRepairableException e) {
                            e.printStackTrace();
                        } catch (GooglePlayServicesNotAvailableException e) {
                            e.printStackTrace();
                        }

                    }
                });


                Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
                dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });




        final String token =sharedPref.getString("my_token", "no token found");
        input_text = (EditText) findViewById(R.id.edittext_chatbox);
        send_text = (Button) findViewById(R.id.button_chatbox_send);
        nameSup = (TextView) findViewById(R.id.nameSup);
        nameSup.setText(sharedPref.getString("sup_name", "name not found"));

        Log.d ("response: ", "im in message page with this many messages: " + Integer.toString(messageList.size()));
        mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
        mMessageRecycler.setHasFixedSize(true);
        mMessageAdapter = new MessageListAdapter(message_list.this, messageList,  sharedPref.getInt("current_user_id", 1));

        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mMessageRecycler.setLayoutManager(mLayoutManager);
        mMessageRecycler.setItemAnimator(new DefaultItemAnimator());
        mMessageRecycler.setAdapter(mMessageAdapter);
        getData();

        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        }, 0, 10, TimeUnit.SECONDS);

        send_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("message :", "in");
                final String temp_m = input_text.getText().toString();
                if (temp_m.length() == 0) return;
                Log.d("message :", temp_m);
                int job_id = sharedPref.getInt("job_id", 1);
                final int sup_id = sharedPref.getInt("sup_id", 1);
                int my_id = sharedPref.getInt("current_user_id", 1);
                final String my_name = sharedPref.getString("my_name", "Ahmad");
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("sender_id", Integer.toString(my_id));
                params.put("job_id", Integer.toString(job_id));
                params.put("receiver_id", Integer.toString(sup_id));
                params.put("message", temp_m);
                params.put("sender_name", my_name);
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                Message temp = new Message();
                temp.setMessage(temp_m);
                temp.setCreatedAt( formatter.format(date));
                User x = new User ();
                x.setUserID(Integer.parseInt(Integer.toString(my_id)));
                x.setName(my_name);
                x.setUserID(my_id);
                temp.setSender(x);
                messageList.add(0, temp);
                mMessageAdapter.notifyDataSetChanged();
                input_text.setText("");

               CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url_post_message, params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response: ", response.toString());
                        try {
                            String temp = response.getString("success");
                            if (temp.equals("0")) {
                                Log.d("Response: ", "failed to send message");
                            } else {
                                Log.d("Response: ", "success, message sent");
                                sendNotificationToappServer(sup_id,sharedPref.getString ("title", "no title"),  my_name + ": " + temp_m);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError response) {
                        Log.d("Response: here", response.toString());
                    }
                });
                Volley.newRequestQueue(message_list.this).add(jsObjRequest);

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                // On complete call either onSignupSuccess or onSignupFailed
                                // depending on success
                                // onSignupFailed();
                            }
                        }, 3000);


            } // on click end bracket
        });


    }



    @Override
    protected void onPause () {
        super.onPause();
        executorService.shutdown();
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST){
            if (resultCode == RESULT_OK){
                Place place = PlacePicker.getPlace(message_list.this, data);
                String link = "http://maps.google.com/maps?q=loc:" + String.format("%f,%f", place.getLatLng().latitude, place.getLatLng().longitude);
                location = link;
               // clearRequest();
            }
        }
    }



    private void getData() {

        StringRequest req = new StringRequest(Request.Method.POST, url_get_messages,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        try {
                            Log.d ("response get: ", "succcess retreived messages");
                            Log.d ("response get", Response);
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response get", Integer.toString(response.length()));
                            messageList.clear();
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                Message temp = new Message();
                                temp.setMessage( person.getString("message"));
                                temp.setCreatedAt( person.getString("created_at"));
                                User x = new User ();
                                x.setUserID(Integer.parseInt(person.getString("sender_id")));
                                x.setName(person.getString("sender_name"));
                                x.setUserID(Integer.parseInt(person.getString("sender_id")));
                                temp.setSender(x);
                                messageList.add (temp);
                                mMessageAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            Log.d ("response: get", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: get", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                int job_id = sharedPref.getInt("job_id", 1);
                int sup_id = sharedPref.getInt("sup_id", 1);
                int my_id = sharedPref.getInt("current_user_id", 1);
                params.put("sender_id", Integer.toString(my_id));
                params.put("job_id", Integer.toString(job_id));
                params.put("receiver_id", Integer.toString(sup_id));
                return params;
            }
        };
        Volley.newRequestQueue(message_list.this).add(req);
    }


    public void sendNotificationToappServer(final int receiver_id, final String title, final String notification){

        StringRequest stringRequest = new StringRequest(Request.Method.POST,SERVER_ADDRESS + Integer.toString(receiver_id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d ("response: ", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("title",title);
                params.put("message",notification);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }



}
