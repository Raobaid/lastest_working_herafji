package com.example.reem.a7erafji;

import android.icu.text.DecimalFormat;
import android.media.Image;
import android.widget.CheckBox;
import android.widget.ImageView;


public class SupplierInfo {

    private String name;
    private String city;
    private double rating;
    private ImageView image;
    private int supID;
    private CheckBox request;
    public void setName (String a){
        this.name = a;
    }
    public void setCity (String a){
        this.city = a;
    }
    public void setRating (Double a){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        Double twoDigitsF = Double.valueOf(decimalFormat.format(a));
        this.rating = twoDigitsF;
    }
    public void setImage (int a){
        this.image.setImageResource(a);
    }
    public void setID (int a){this.supID = a;}
    public String getName (){
        return this.name;
    }
    public String getCity (){
        return this.city;
    }
    public Double getRating (){
        return this.rating;
    }
    public ImageView getImage(){
        return this.image;
    }
    public int getSupID(){return this.supID;}
}
