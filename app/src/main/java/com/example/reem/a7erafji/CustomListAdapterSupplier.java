package com.example.reem.a7erafji;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import java.util.ArrayList;

public class CustomListAdapterSupplier extends BaseAdapter {
    private ArrayList<search_supplier> listData;
    private LayoutInflater layoutInflater;
    Context temp;
    public CustomListAdapterSupplier(Context aContext, ArrayList<search_supplier> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        temp = aContext;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public final Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public final long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final CustomListAdapterSupplier.ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.search_supplier, null);
            holder = new CustomListAdapterSupplier.ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.nameSup);
            holder.city = (TextView) convertView.findViewById(R.id.citySup);
            holder.rating = (RatingBar) convertView.findViewById(R.id.ratingBar);
            holder.image = (ImageView) convertView.findViewById(R.id.imageSup);
            convertView.setTag(holder);
        } else {
            holder = (CustomListAdapterSupplier.ViewHolder) convertView.getTag();
        }

        holder.name.setText(listData.get(position).getName());
        holder.city.setText(listData.get(position).getCity());
        holder.rating.setRating(listData.get(position).getRating());
        String url = "http://192.168.91.1/android_connect/upload/";
        url = url + listData.get(position).getId() +".jpg";
        ImageRequest imgReq = new ImageRequest(url, new Response.Listener<Bitmap>(){

            @Override
            public void onResponse(Bitmap response){
                holder.image.setImageBitmap(response);
            }

        }, 0,0,ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.image.setImageResource(R.drawable.man);
            }
        });
        MySingleton.getInstance(temp).addToRequestQueue(imgReq);

//        holder.name.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view){
//                editor.putInt("profile_id", sharedPref.getInt("current_user_id", 1));
//                editor.apply();
////                Intent intent = new Intent(Main_Page.this, Profile.class);
////                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
////                Main_Page.this.startActivity(intent);
//            }
//        });


        return convertView;
    }

    static class ViewHolder {
        TextView name, city;
        RatingBar rating;
        ImageView image;
    }

}
