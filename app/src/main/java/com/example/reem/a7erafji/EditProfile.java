package com.example.reem.a7erafji;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditProfile extends AppCompatActivity {

    private final int IMG_REQUEST = 1;
    Bitmap bitmap;
    EditText Name, Location;
    ImageView imgv;
    Button upload, cancel , edit;
    private String CurUser, Profile_id;
    Boolean flag =false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        edit = (Button) findViewById(R.id.edit);
        cancel = (Button) findViewById(R.id.cancel);
        upload = (Button) findViewById(R.id.img);
        Name = (EditText) findViewById(R.id.name);
        Location = (EditText) findViewById(R.id.location);
        imgv = (ImageView) findViewById(R.id.imagV);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(EditProfile.this);
        SharedPreferences.Editor editor = sharedPref.edit();
        CurUser =  Integer.toString(sharedPref.getInt("current_user_id", 1));
        Profile_id =  Integer.toString(sharedPref.getInt("profile_id", 1));
        editor.apply();

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = true;
                selectImg();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(getApplicationContext(),Profile.class);
                startActivity(i);
            }
        });

        edit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view){
                String URL = "http://192.168.91.1/android_connect/edit.php";
                StringRequest req = new StringRequest(Request.Method.POST, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String Response) {
                                Log.d ("response edit: ", "succcess");
                                Log.d ("response", Response);
                                if (flag){
                                    uploadImg();
                                }else{
                                    Intent i = new Intent(getApplicationContext(),Profile.class);
                                    startActivity(i);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d ("response: ", "error");
                                error.printStackTrace();
                            }
                        }
                ) {
                    // here is params will add to your url using post method
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("id", Profile_id);
                        params.put("name", Name.getText().toString());
                        params.put("location", Location.getText().toString());
                        return params;
                    }
                };
                // Adding request to request queue
                Volley.newRequestQueue(EditProfile.this).add(req);

            }
        });
    }

    private void selectImg(){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMG_REQUEST && resultCode== RESULT_OK && data!= null){
            Uri path = data.getData();
            try {
                bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                imgv.setImageBitmap(bitmap);
                imgv.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImg(){

        String URL = "http://192.168.91.1/android_connect/uploadImg.php";
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        Log.d ("response edit: ", "succcess");
                        Log.d ("response", Response);
                        Intent i = new Intent(getApplicationContext(),Profile.class);
                        startActivity(i);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", Profile_id);
                params.put("image_data" , imgToStrinng(bitmap));
                return params;
            }
        };
        // Adding request to request queue
        Volley.newRequestQueue(EditProfile.this).add(req);

    }

    private  String imgToStrinng (Bitmap bitmap){
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArray);
        byte [] imgBytes = byteArray.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }
}

