package com.example.reem.a7erafji;

import android.icu.text.DecimalFormat;
import android.media.Image;
import android.widget.CheckBox;
import android.widget.ImageView;


public class jobInfo{

    private String title;
    private String SupName;
    private Float Suprate;
    private String deadline;
    private String category;
    private String description;
    private int jobID;
    private int visitID;
    private int SupID;
    private float r1;
    private float r2;
    private float r3;

    public void setTitle(String a){this.title = a;}
    public void setSupName(String a){this.SupName = a;}
    public void setSuprate(Float a){
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        float twoDigitsF = Float.valueOf(decimalFormat.format(a));
        this.Suprate = twoDigitsF;}
    public  void setDeadline(String a){this.deadline = a;}
    public void setCategory(String a){this.category = a;}
    public  void setDescription(String a){this.description = a;}
    public void setJobID(int a){this.jobID = a;}
    public void setVisitID(int a){this.visitID = a;}
    public void setSupID(int a){this.SupID = a;}
    public  Float getR1(){return this.r1; }
    public  Float getR2(){return this.r2; }
    public  Float getR3(){return this.r3; }
    public void setR1(Float a){this.r1 = a;}
    public void setR2(Float a){this.r2 = a;}
    public void setR3(Float a){this.r3 = a;}
    public  String getTitle(){return this.title; }
    public  String getSupName(){return this.SupName; }
    public  Float getSuprate(){return this.Suprate; }
    public  String getDeadline(){return this.deadline; }
    public String getCategory(){return this.category;}
    public String getDescription(){return this.description;}
    public int getJobID(){return this.jobID;}
    public int getVisitID(){return this.visitID;}
    public int getSupID(){return this.SupID;}

}
