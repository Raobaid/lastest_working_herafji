package com.example.reem.a7erafji;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class view_suppliers extends AppCompatActivity {


    RatingBar rbar;
    TextView category;
    private SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    ListView lv;
    int catId;
    float ratingMin=0;
    ArrayList<search_supplier> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_suppliers);
        rbar = (RatingBar) findViewById(R.id.ratingBar);
        category = (TextView) findViewById(R.id.category);
        lv = (ListView) findViewById (R.id.custom_list);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(view_suppliers.this);
        editor = sharedPref.edit();
        category.setText(sharedPref.getString("category", "no category found"));
        catId = sharedPref.getInt("categoryID", 1);
        ratingMin = rbar.getRating();
        getData();
        // rating bar on change listenser
        rbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar,
                                        float rating, boolean fromUser) {
                ratingMin = rating;
                getData();
            }
        });

    }

    void getData(){
        list.clear();
        final String URL = "http://192.168.91.1/android_connect/search_suppliers.php";
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        try {
                            Log.d ("response: ", "succcess");
                            Log.d ("response", Response);
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response ", Integer.toString(response.length()));
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                search_supplier  temp = new search_supplier();
                                temp.setRating(Float.parseFloat(person.getString ("overallRating")));
                                temp.setName(person.getString("name"));
                                temp.setCity(person.getString("Location"));
                                temp.setId(Integer.parseInt(person.getString("id")));
                                list.add (temp);
                            }
                            final CustomListAdapterSupplier clad = new CustomListAdapterSupplier(view_suppliers.this, list);
                            lv.setAdapter(clad);

                        } catch (JSONException e) {
                            Log.d ("response: ", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("catID", Integer.toString(catId));
                params.put("min", Float.toString(ratingMin));
                return params;
            }
        };
        Volley.newRequestQueue(view_suppliers.this).add(req);

    }
}
