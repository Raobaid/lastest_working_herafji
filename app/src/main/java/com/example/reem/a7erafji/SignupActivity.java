package com.example.reem.a7erafji;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";

    EditText _nameText, _phoneNumber, email, password, confirmPassword;
    Button _signupButton;
    TextView _loginLink, mTxtDisplay;
    RadioButton supplierB, customerB;
    RadioGroup radg;
    Spinner dropdown;
    String cat="0";
    private static final int REQUEST_LOGIN = 0;
    // Progress Dialog
    private ProgressDialog pDialog;

    // variable for signup validation
    private boolean valid;

    // url to add phone validation
    //  private static String url_validpn = "http://192.168.91.1/android_connect/phone_valid.php";

    // url to add user info singup
    private static String url_signup = "http://192.168.91.1/android_connect/sign_up.php";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        _nameText = (EditText) findViewById(R.id.input_name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        _phoneNumber = (EditText) findViewById(R.id.phone_number);
        _signupButton = (Button) findViewById(R.id.btn_signup);
        _loginLink = (TextView) findViewById(R.id.link_login);
        radg = (RadioGroup) findViewById(R.id.radg);
        supplierB = (RadioButton) findViewById(R.id.supplierB);
        customerB = (RadioButton) findViewById(R.id.customerB);
        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        confirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();

            }
        });
        dropdown = (Spinner) findViewById(R.id.spinner1);
        String[] items = new String[]{"Computer Technician", "House Painting or Decorating", "Electrician", "Plumber or Water Technician", "Home Appliances Technician", "House Cleaning"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        supplierB.setOnClickListener(new View.OnClickListener()
                                     {
                                         @Override
                                         public void onClick(View view) {
                                             dropdown.setVisibility(View.VISIBLE);
                                         }
                                     }
        );

        customerB.setOnClickListener(new View.OnClickListener()
                                     {
                                         @Override
                                         public void onClick(View view) {
                                             dropdown.setVisibility(View.INVISIBLE);
                                         }
                                     }
        );


        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, REQUEST_LOGIN);
            }
        });
    }


    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }
        Log.d("Response: ", "here after validation");
        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        String name = _nameText.getText().toString();
        String phone = _phoneNumber.getText().toString();
        String _email = email.getText().toString();
        String _password = password.getText().toString();
        if (supplierB.isChecked())
            cat = Long.toString(dropdown.getSelectedItemId()+1);

        // add user info to the database
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("phone_number", phone);
        params.put("name", name);
        params.put("email", _email);
        params.put("password", _password);
        params.put("category", cat);
        Toast.makeText(getBaseContext(), cat, Toast.LENGTH_LONG).show();
        valid = true;
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url_signup, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response: ", response.toString());
                try {
                    String temp =  response.getString("success");
                    //  Toast.makeText(getBaseContext(), temp, Toast.LENGTH_LONG).show();
                    if (temp.equals("0")) {
                        Log.d("Response: ", "failed");
                        onSignupFailed();
                        valid = false;
                    }
                    else{
                        Log.d("Response: ", "success");
                        Toast.makeText(getBaseContext(), temp, Toast.LENGTH_LONG).show();
                        String CurId = response.getString("id");
                        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(SignupActivity.this);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt("current_user_id", Integer.parseInt(CurId));
                        editor.apply();
                        onSignupSuccess();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    onSignupFailed();
                    valid = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError response) {
                Log.d("Response: ", response.toString());
            }
        });
        Volley.newRequestQueue(this).add(jsObjRequest);

    }


    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);

            Intent i = new Intent(getApplicationContext(),Main_Page.class);
            startActivity(i);

    }

    public void onSignupFailed() {
        //  Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        //    this.finish();
        _signupButton.setEnabled(true);
    }


    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String phone = _phoneNumber.getText().toString();
        String _email = email.getText().toString();
        String _password = password.getText().toString();
        String _ConfirmPassword = confirmPassword.getText().toString();

        if (name.isEmpty() || name.length() < 2) {
            _nameText.setError("at least 2 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (_email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(_email).matches()) {
            email.setError("Did not enter an email");
            valid = false;
        } else {
            email.setError(null);
        }

        if (! _password.equals(_ConfirmPassword)) {
            password.setError("Password don't match confirm password");
            valid = false;
        } else {
            password.setError(null);
        }
        if (phone.isEmpty() || phone.length() < 10 ){
            _phoneNumber.setError("Enter a valid phone number");
            valid = false;
        }else{
            _phoneNumber.setError(null);
        }


        return valid;
    }
}

