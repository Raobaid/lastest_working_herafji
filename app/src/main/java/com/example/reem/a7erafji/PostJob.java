package com.example.reem.a7erafji;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PostJob extends AppCompatActivity {

    private static final String TAG = "JobPost";
    private TextView DisplayDate;
    private EditText title, desc;
    private DatePickerDialog.OnDateSetListener DateSetListener;
    private SharedPreferences sharedPref;
    private static String url_post_job = "http://192.168.91.1/android_connect/post_job.php";
    private SharedPreferences.Editor editor;
    Button submit;
    ListView lv;
    CustomListAdapter clad;
    int catId;
    ArrayList <Integer> UserChoices = new ArrayList<>();
    ArrayList image_details = new ArrayList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        setContentView(R.layout.activity_post_job2);
        catId = sharedPref.getInt("categoryID", 1);
        DisplayDate = (TextView)findViewById(R.id.date);
        title = (EditText) findViewById(R.id.title);
        desc = (EditText) findViewById(R.id.desc);
        submit = (Button) findViewById(R.id.post_job_btn);
        // view list
        lv = (ListView) findViewById(R.id.custom_list);
        getListData();
        DisplayDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Calendar cal = Calendar.getInstance();
                int year = cal.get (Calendar.YEAR);
                int month = cal.get (Calendar.MONTH);
                int day = cal.get (Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(PostJob.this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,DateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        DateSetListener = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet (DatePicker datePicker, int y, int m, int d){
                m = m +1;
                String date = String.valueOf(m)+ '/' + String.valueOf(d) + '/' + String.valueOf(y) ;
                DisplayDate.setText(date);
            }
        };

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserChoices.clear();
                for(int i=0; i<image_details.size(); i++) {
                    if (clad.checkedHolder[i]) UserChoices.add(clad.supIds[i]);
                }
                if (UserChoices.size() ==0){
                    Toast.makeText(PostJob.this, "No Suppliers requested!" ,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if (desc.getText().length() ==0){
                    Toast.makeText(PostJob.this, "No Description added!" ,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if (title.getText().length() ==0){
                    Toast.makeText(PostJob.this, "No title added!" ,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(PostJob.this, UserChoices.size() + " Suppliers requested" ,
                        Toast.LENGTH_LONG).show();


                HashMap<String, String> params = new HashMap<String, String>();
                params.put("title", title.getText().toString());
                params.put("desc", desc.getText().toString());
                String temp = DisplayDate.getText().toString();
                if (temp.length() ==0)temp = "Open Ended Date";
                params.put ("date", temp);
                params.put("user_id",  Integer.toString(sharedPref.getInt("current_user_id", 1)));
                params.put ("category",sharedPref.getString("Category", "category"));

                // take all supplier ids and merge them with spaces to be seperated later after key is assigned for job post
                String supplier = "";
                for (Integer X: UserChoices){
                    supplier += X.toString()  + ' ';
                }
                supplier = supplier.substring(0, supplier.length() -1);
                params.put ("suppliers", supplier);
                boolean valid = true;
                CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url_post_job, params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response: ", response.toString());
                        try {
                            String temp =  response.getString("success");
                            if (temp.equals("0")) {
                                Log.d("Response: ", "failed");
                            }
                            else{
                                Toast.makeText(PostJob.this, "Job post added!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), Main_Page.class);
                                startActivityForResult(intent, 0);
                                Log.d("Response: ", "success");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError response) {
                        Log.d("Response: ", response.toString());
                    }
                });
                Volley.newRequestQueue(PostJob.this).add(jsObjRequest);

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                // On complete call either onSignupSuccess or onSignupFailed
                                // depending on success
                                // onSignupFailed();
                            }
                        }, 3000);


            } // on click end bracket
        });


    }

    private void getListData() {
        image_details.clear();
        final String URL = "http://192.168.91.1/android_connect/search_suppliers.php";
        StringRequest req = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String Response) {
                        try {
                            Log.d ("response: ", "succcess");
                            Log.d ("response", Response);
                            JSONArray response = new JSONArray(Response);
                            Log.d ("response ", Integer.toString(response.length()));
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject person = (JSONObject) response
                                        .get(i);
                                SupplierInfo temp = new SupplierInfo();
                                temp.setRating(Double.parseDouble(person.getString ("overallRating")));
                                temp.setName(person.getString("name"));
                                temp.setCity(person.getString("Location"));
                                temp.setID(Integer.parseInt(person.getString("id")));
                                image_details.add (temp);
                            }
                            Toast.makeText(PostJob.this, "Number of Suppliers in Category: " + image_details.size(),
                                    Toast.LENGTH_SHORT).show();
                            clad = new CustomListAdapter(PostJob.this, image_details);
                            lv.setAdapter(clad);
                        } catch (JSONException e) {
                            Log.d ("response: ", "error parse");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d ("response: ", "error");
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("catID", Integer.toString(catId));
                params.put("min", Integer.toString(0));
                return params;
            }
        };
        Volley.newRequestQueue(PostJob.this).add(req);
    }
}
