package com.example.reem.a7erafji;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class CustomListAdapterJob extends BaseAdapter {

    private ArrayList<jobInfo> listData;
    private LayoutInflater layoutInflater;
    private static final int VIEW_JOBPOST = 3;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    Context temp;
    public CustomListAdapterJob(Context aContext, ArrayList<jobInfo> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(aContext);
        editor = sharedPref.edit();
        temp = aContext;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public final Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public final long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final CustomListAdapterJob.ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.my_job, null);
            holder = new CustomListAdapterJob.ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.description= (TextView) convertView.findViewById(R.id.description);
            holder.category = (TextView) convertView.findViewById(R.id.category);
            holder.deadline = (TextView) convertView.findViewById(R.id.deadline);
            holder.viewMore = (Button) convertView.findViewById(R.id.viewMore);
            holder.deleteBt = (ImageView) convertView.findViewById(R.id.deleteBt);
            convertView.setTag(holder);
        } else {
            holder = (CustomListAdapterJob.ViewHolder) convertView.getTag();
        }

        holder.title.setText(listData.get(position).getTitle());
        holder.description.setText(listData.get(position).getDescription());
        holder.deadline.setText(listData.get(position).getDeadline());
        holder.category.setText(listData.get(position).getCategory());

        holder.viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // go to specific page of job post
                editor.putString ("title", listData.get(position).getTitle());
                Log.d ("title: ", listData.get(position).getTitle());
                editor.putString("deadline", listData.get(position).getDeadline());
                editor.putString("description", listData.get(position).getDescription());
                editor.putString("category", listData.get(position).getCategory());
                editor.putInt("job_id", listData.get(position).getJobID());
                editor.apply();
                Intent intent = new Intent(temp, View_Job.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                temp.startActivity(intent);
            }
        });

        holder.deleteBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(temp)
                        .setTitle("Title")
                        .setMessage("Are you sure you want to delete this job?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                String url = "http://192.168.91.1/android_connect/delete_job.php";
                                StringRequest req = new StringRequest(Request.Method.POST, url,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String Response) {
                                                Log.d ("deleted:", "succcess");
                                                Log.d ("response", Response);
                                                Toast.makeText(temp, "Job was deleted.", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(temp, Main_Page.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                                temp.startActivity(intent);
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Log.d ("deleted:", "error");
                                                error.printStackTrace();
                                            }
                                        }
                                ) {
                                    // here is params will add to your url using post method
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<>();
                                        Log.d ("deleted: ", Integer.toString(listData.get(position).getJobID()));
                                        params.put("id", Integer.toString(listData.get(position).getJobID()));
                                        return params;
                                    }
                                };
                                // Adding request to request queue
                                Volley.newRequestQueue(temp).add(req);
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView title;
        TextView description;
        TextView deadline;
        TextView category;
        ImageView deleteBt;
        Button viewMore;
    }
}
