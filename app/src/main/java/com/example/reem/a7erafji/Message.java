package com.example.reem.a7erafji;

/**
 * Created by Reem on 5/11/2018.
 */

public class Message {
    String message;
    User sender;
    String createdAt;
    void setMessage(String a){this.message = a;}
    void setSender (User a){this.sender = a;}
    void setCreatedAt(String a){this.createdAt = a;}
    String getMessage(){return this.message;}
    User getSender(){return this.sender;}
    String getCreatedAt(){return this.createdAt;}

}
